-----------------------------------------------------------
S/P Magic Menus for Drupal
  by Jeff Robbins
    jeff -/a@t\- lullabot.com

Description: ----------------------------------------------
This module creates DHTML/AJAX menu blocks in three different styles:
1) Expandable - uses Scriptaculous' sliding blind animations to allow dynamic viewing of submenu items.
2) Floating Absolute - pulls the block out of the page flow and styles it appropriately. The menu can be dragged anywhere on the page and will remain there as you move from page to page.
3) Floating Fixed - is the same as absolute, but the block does not scroll with the page.

Please note that these menu blocks are *completely* backward compatible with non-javascript browsers. The only trade off is that all submenu items will be expanded.

This module REQUIRES the S/P Ajax module.

Installation: ---------------------------------------------

1) Place the directory containing this document in your Drupal modules directory.

2) Enable this module at admin/modules

3) Go to admin/settings/spajax/magicmenus to set up.
You will need to expand all of the submenus of any menus for which you would like to affect. These menus should show up at the bottom of the settings form with a message if they have submenu items that are not expanded. Click the link and all submenus will be set to "expanded".